from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.get_all, name='index'),
    url(r'^(?P<hack_id>[0-9]+)/$', views.get_by_id, name='detail'),
]