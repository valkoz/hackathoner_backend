from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder

from hackathoner.models import Hack


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Hack):
            return force_text(obj)
        return super(LazyEncoder, self).default(obj)
