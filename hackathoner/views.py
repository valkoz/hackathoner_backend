from django.http import HttpResponse
from hackathoner.models import Hack
from django.core import serializers
import json


def get_all(request):
    # TODO json format {pk, name, link, description}. Now there is no pk
    raw_data = serializers.serialize('python', Hack.objects.all())
    actual_data = [d['fields'] for d in raw_data]
    output = json.dumps(actual_data, ensure_ascii=False)
    return HttpResponse(output, content_type="application/json")


def get_by_id(request, hack_id):
    raw_data = serializers.serialize('python', Hack.objects.filter(pk=hack_id))
    actual_data = [d['fields'] for d in raw_data]
    output = json.dumps(actual_data, ensure_ascii=False)
    return HttpResponse(output, content_type="application/json")


