from django.db import models


class Hack(models.Model):

    name = models.CharField(max_length=100, default="")
    description = models.CharField(max_length=300, default="")
    img_link = models.CharField(max_length=100, default="")
    site_link = models.CharField(max_length=100, default="")
    begin_date = models.CharField(max_length=20, default="10.10.2017")
    end_date = models.CharField(max_length=20, default="10.10.2017")
    start_time = models.CharField(max_length=5, default="12:00")
    end_time = models.CharField(max_length=5, default="12:00")
    place = models.CharField(max_length=100, default="")
    coordinateX = models.FloatField(default=55.839950)
    coordinateY = models.FloatField(default=37.490334)
    full_description = models.CharField(max_length=1000, default="")
    status = models.CharField(max_length=10, default="online")


