# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hack_name', models.CharField(max_length=100)),
                ('hack_description', models.CharField(max_length=300)),
                ('hack_imglink', models.CharField(max_length=100)),
            ],
        ),
    ]
